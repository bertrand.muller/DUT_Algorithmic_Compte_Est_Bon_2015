import java.util.ArrayList;
import java.util.Collections;


/**
 * Classe permettant la resolution du "Compte Est Bon"
 * 
 * @author Florian LEDOUX
 * 		   Bertrand MULLER
 * 		   Groupe S3D
 */
public class CompteEstBon {

	
	/**
	 * Variable statique contenant le resultat a trouver
	 */
	private static int RESULTAT;
	
	
	/**
	 * Variable statique booleenne permettant de savoir
	 * si le "Compte Est Bon" a ete trouve
	 */
	private static boolean FIN;
	
	
	/**
	 * Variable statique entiere permettant d'enregistrer
	 * le nombre le plus proche du RESULTAT cherche
	 * (le compte n'est pas bon)
	 */
	private static int PROCHE = 0;
	
	
	/**
	 * Variable statique enregistrant la liste des calculs
	 * sous forme de chaine de caracteres
	 */
	private static String LISTE_CALCULS = "";

	
	/**
	 * Methode principale permettant le lancement du programme
	 * de resolution du "Compte Est Bon"
	 * 
	 * @param args
	 * 		Ensemble des nombres a utiliser
	 * 		Resultat a trouver
	 */
	public static void main(String[] args) {

		
		//Utilisation d'une liste d'entiers
		ArrayList<Integer> nbs = new ArrayList<Integer>();

		
		//Ajout de l'ensemble des nombres en parametre dans la liste
		for (String i : args) {

			nbs.add(Integer.parseInt(i));

		}

		
		nbs.remove(6);

		
		//Ajout du resultat dans la variable statique RESULTAT
		RESULTAT = Integer.parseInt(args[6]);
		
		System.out.println("Liste de nombres au d�part : " + nbs.toString() + "\n");
		
		//Tri de la liste
		trierListe(nbs);

		
		//Test pour savoir si le "Compte Est Bon" a ete trouve
		if (trouverCompte(nbs)) {

			System.out.print("Le compte est bon !! \n" + LISTE_CALCULS);

		} else {

			System.out.println("Pas de solution exacte (pour " + RESULTAT + ").");
			
			RESULTAT = PROCHE;
			trouverCompte(nbs);

			System.out.println("La valeur la plus proche : " + PROCHE + "\n" + LISTE_CALCULS);

		}

	}

	
	/**
	 * Methode statique permettant de resoudre
	 * le probleme du "Compte Est Bon"
	 * 
	 * @param nbs
	 * 		Liste de nombres a utiliser
	 * 
	 * @return
	 * 		Booleen a 'true' si le RESULTAT a ete trouve 
	 */
	public static boolean trouverCompte(ArrayList<Integer> nbs) {

		
		//Tableau d'operateurs
		String[] operateurs = { "*", "+", "-", "/" };
		
		
		int i = 0;

		
		//On verifie qu'il y a au moins deux nombres dans la liste
		if (nbs.size() >= 2) {

			
			//On parcourt l'ensemble des nombres
			while ((i < nbs.size() - 1) && (!FIN)) {

				int j = i + 1;

				
				//On realise tous les couples de nombres possibles
				while ((j < nbs.size()) && (!FIN)) {

					
					//On selectionne les deux nombres
					int nb1 = nbs.get(i);
					int nb2 = nbs.get(j);
					
					
					int k = 0;

					
					//On fait l'ensemble des operations sur chacun des couples
					while ((k < operateurs.length) && (!FIN)) {

						
						//On calcule le resultat entre les deux nombres selon un operateur
						int res = calculerResultat(operateurs[k], nb1, nb2);

						
						//On verifie que le nombre res est plus proche ou non de RESULTAT que PROCHE
						if (Math.abs(RESULTAT - PROCHE) > Math.abs(RESULTAT- res)) {

							PROCHE = res;

						}

						
						//On verifie que l'on ne situe pas dans le cas d'une operation inutile
						if (res != -1) {

							
							//On cree une nouvelle liste avec res et on la trie
							ArrayList<Integer> nouv_liste = ajouterResultatListe(nbs, nb1, nb2, res);
							trierListe(nouv_liste);

							
							//On lance l'appel recursif
							FIN = trouverCompte(nouv_liste);
							
							
							//On teste la nouvelle valeur de FIN
							if (FIN) {

								
								//On met a jour la variable LISTE_CALCULS
								LISTE_CALCULS = nb1 + operateurs[k] + nb2+ " = " + res + "\n" + LISTE_CALCULS;
								
								
								return true;

							}

						}

						k++;

					}

					j++;

				}

				i++;

			}

			return false;

		} else {

			if (nbs.get(0) == RESULTAT) {

				return true;

			} else {

				return false;

			}

		}

	}

	
	/**
	 * Methode statique retournant une nouvelle liste apres avoir ajoute 
	 * la valeur du resultat d'une operation entre deux nombres a l'ancienne
	 * 
	 * @param nbs
	 * 		Liste de nombres initiale
	 * 
	 * @param nb1
	 * 		Premier nombre
	 * 
	 * @param nb2
	 * 		Deuxieme nombre
	 * 
	 * @param res
	 * 		Valeur du resultat de l'operation entre nb1 et nb2
	 * 
	 * @return
	 * 		Nouvelle liste avec les anciens nombres et la valeur de res
	 */
	private static ArrayList<Integer> ajouterResultatListe(ArrayList<Integer> nbs, int nb1, int nb2, int res) {

		
		//Declaration d'une nouvelle liste
		ArrayList<Integer> nouv_liste = new ArrayList<Integer>();

		
		//On ajoute l'ensemble des anciens nombres
		for (int i : nbs) {

			nouv_liste.add(i);

		}

		
		//On teste qu'il existe au moins un element dans la nouvelle liste
		if (nouv_liste.size() > 1) {

			nouv_liste.remove((Integer) nb1);
			nouv_liste.remove((Integer) nb2);
			nouv_liste.add(res);

		} else {

			nouv_liste.remove(0);
			nouv_liste.add(res);
		}

		
		return nouv_liste;
		
	}

	
	/**
	 * Methode statique permettant de renvoyer le resultat
	 * de l'operation entre deux nombres
	 * 
	 * @param op
	 * 		Operande de l'operation
	 * 
	 * @param nb1
	 * 		Premier nombre
	 * 
	 * @param nb2
	 * 		Deuxieme nombre
	 * 
	 * @return
	 * 		Valeur du resultat de l'operation entre nb1 et nb1 selon l'operande op
	 * 		Cette valeur vaut -1 si l'operation est inutile
	 */
	private static int calculerResultat(String op, int nb1, int nb2) {

		
		//Valeur par defaut du resultat
		int res = -1;

		
		//On teste la valeur de l'operande
		//Et pour chaque cas, on verifie que l'on ne se situe pas dans un cas inutile
		switch (op) {

		case "*":

			if (nb2 != 1 && nb1 != 1) {
				res = nb1 * nb2;
			}

			break;

		case "+":

			res = nb1 + nb2;

			break;

		case "-":

			res = nb1 - nb2;

			break;

		case "/":

			if (nb2 != 0 && nb2 != 1) {

				if (nb1 % nb2 == 0) {
					
					res = nb1 / nb2;
					
				}
				
			}
			
			break;

		}
		
		return res;
		
	}

	
	/**
	 * Methode statique permettant de trier la liste passee en parametre
	 * 
	 * @param nbs
	 * 		Liste de nombres a trier
	 */
	private static void trierListe(ArrayList<Integer> nbs) {

		Collections.sort(nbs, Collections.reverseOrder());

	}
	
}
